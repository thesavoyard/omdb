package com.example.omdb.view.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.omdb.databinding.FirstFragementBinding
import com.example.omdb.model.OMDBRepo
import com.example.omdb.model.local.FavoriteMovies
import com.example.omdb.view.adapters.FirstFragAdapter
import com.example.omdb.view.state.MoviesState
import com.example.omdb.viewmodel.MoviesVMFactory
import com.example.omdb.viewmodel.MoviesViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class FirstFragment : Fragment() {
    private var _binding: FirstFragementBinding? = null
    private val binding get() = _binding!!
    @Inject
    lateinit var viewModel: MoviesViewModel
    lateinit var moviesState: MoviesState
    private val theAdapter: FirstFragAdapter by lazy {
        FirstFragAdapter(
            viewModel::saveFavoriteMovies
        ) { id: String ->
            val action =
                FirstFragmentDirections.actionFirstFragmentToOneMovieFragment(id)
            findNavController().navigate(action)
        }
    }



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FirstFragementBinding.inflate(
        inflater, container, false
    ).also {
        _binding = it
        initViews()

    }.root

    private val listener = object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String): Boolean {
            binding.svFirstFrag.clearFocus()
            viewModel.getMovies(query)
            return true
        }

        override fun onQueryTextChange(newText: String?): Boolean {
            //Log.e("TAG", "onQueryTextChange: $newText", )
            return true
        }
    }

    fun initViews() {
        viewModel.movies.observe(viewLifecycleOwner) { state ->
            var searchResult = state.searchResults


            with(binding.rvFirstFrag) {
                adapter = theAdapter
                layoutManager = LinearLayoutManager(context)
            }
            theAdapter.updateList(searchResult)
        }

        binding.svFirstFrag.setOnQueryTextListener(listener)

        binding.btnSeeFavorites.setOnClickListener {
            val action = FirstFragmentDirections
                .actionFirstFragmentToFavoritesFragment()
            findNavController().navigate(action)
        }
    }


}