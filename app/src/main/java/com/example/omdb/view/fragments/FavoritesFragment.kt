package com.example.omdb.view.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.appcompat.widget.SearchView
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.omdb.databinding.FavoritesFragmentBinding
import com.example.omdb.model.OMDBRepo
import com.example.omdb.view.adapters.FavoritesFragAdapter
import com.example.omdb.view.state.MoviesState
import com.example.omdb.viewmodel.MoviesVMFactory
import com.example.omdb.viewmodel.MoviesViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class FavoritesFragment: Fragment() {
    private var _binding: FavoritesFragmentBinding? = null
    private val binding get() = _binding!!
    @Inject
    lateinit var viewModel: MoviesViewModel
    lateinit var moviesState: MoviesState
    private val theAdapter: FavoritesFragAdapter by lazy {
        FavoritesFragAdapter(
            viewModel::deleteMovie
        ) { id: String ->
            val action =
                FavoritesFragmentDirections.actionFavoritesFragmentToOneMovieFragment(id)
            findNavController().navigate(action)

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FavoritesFragmentBinding.inflate(
        inflater, container, false
    ).also {
        _binding = it
        viewModel.getFavoriteMovies()
        initViews()
    }.root


    private val listener = object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(search: String): Boolean {
            binding.svFavoritesFrag.clearFocus()
            viewModel.getFilteredFavorites(search)
//            Log.e("TAG", "onQueryTextSubmit search term: $search ", )
            return true
        }

        override fun onQueryTextChange(newText: String?): Boolean {
//            Log.e("TAG", "onQueryTextChange: $newText", )
            return true
        }
    }

    fun initViews() {
        viewModel.movies.observe(viewLifecycleOwner) { state ->
            var favoriteMovies = state.favoriteMovies

            with(binding.rvFavoritesFrag) {
                adapter = theAdapter
                layoutManager = LinearLayoutManager(context)
            }

            theAdapter.updateList(favoriteMovies)
            binding.svFavoritesFrag.setOnQueryTextListener(listener)
        }
    }
}