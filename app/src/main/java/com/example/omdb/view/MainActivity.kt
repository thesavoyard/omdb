package com.example.omdb.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.omdb.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity(R.layout.activity_main)