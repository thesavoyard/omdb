package com.example.omdb.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import coil.load
import com.example.omdb.databinding.OneMovieFragmentBinding
import com.example.omdb.view.state.MoviesState
import com.example.omdb.viewmodel.MoviesVMFactory
import com.example.omdb.viewmodel.MoviesViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class OneMovieFragment : Fragment() {

    val args by navArgs<OneMovieFragmentArgs>()
    private var _binding: OneMovieFragmentBinding? = null
    private val binding get() = _binding!!
    lateinit var controller: NavController
    lateinit var moviesState: MoviesState
    @Inject
    lateinit var viewModel: MoviesViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = OneMovieFragmentBinding.inflate(inflater, container, false)
        controller = findNavController()
        val movieId = args.movie
        viewModel.getOneMovie(movieId)
        initViews()
        return binding.root
    }

    fun initViews() {
        viewModel.movies.observe(viewLifecycleOwner) { state ->
            with(binding) {
                ivMovieItem.load(state.oneMovie?.Poster)
                tvTitle.text = state.oneMovie?.Title
                tvRating.text = state.oneMovie?.Rated
                tvRating.text = state.oneMovie?.Year
                tvGenre.text = state.oneMovie?.Genre
                tvType.text = state.oneMovie?.Type
                tvPlot.text = state.oneMovie?.Plot
            }

        }
    }
}