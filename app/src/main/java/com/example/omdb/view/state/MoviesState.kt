package com.example.omdb.view.state

import com.example.omdb.model.local.FavoriteMovies
import com.example.omdb.model.remote.objects.IMDBResponse
import com.example.omdb.model.remote.objects.Search
import com.example.omdb.model.remote.objects.SearchResults

data class MoviesState(
    val isLoading: Boolean = false,
    val errorMsg:String? = "You really screwed that up didn't you?",
    val favoriteMovies: List<FavoriteMovies> = listOf(),
    val searchResults: List<Search> = listOf(),
    val oneMovie: IMDBResponse? = null
)