package com.example.omdb.view.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.omdb.databinding.MovieItemBinding
import com.example.omdb.model.local.FavoriteMovies
import androidx.navigation.fragment.findNavController
import com.example.omdb.R
import com.example.omdb.view.fragments.FavoritesFragmentDirections

class FavoritesFragAdapter(
    val onDeleteFavorite: (movies: FavoriteMovies) -> Unit,
    val navigateToOneMovie: (id: String) -> Unit
) : RecyclerView.Adapter<FavoritesFragAdapter.FavoritesFragViewHolder>() {

    private var favoriteMovies: MutableList<FavoriteMovies> = mutableListOf()

    fun updateList(newList: List<FavoriteMovies>) {
        val oldSize = favoriteMovies.size
        favoriteMovies.clear()
        notifyItemRangeChanged(0, oldSize)
        favoriteMovies.addAll(newList)
        notifyItemRangeChanged(0, newList.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoritesFragViewHolder {
        return FavoritesFragViewHolder(
            MovieItemBinding.inflate(LayoutInflater.from(parent.context), parent, false),
            onDeleteFavorite
        )
    }

    override fun onBindViewHolder(holder: FavoritesFragViewHolder, position: Int) {
        holder.displayMovies(favoriteMovies[position], navigateToOneMovie)
    }

    override fun getItemCount(): Int = favoriteMovies.size

    class FavoritesFragViewHolder(
        private val binding: MovieItemBinding,
        val onDeleteFavorite: (movies: FavoriteMovies) -> Unit

    ) :
        RecyclerView.ViewHolder(binding.root) {
        lateinit var controller: NavController
        fun displayMovies(movie: FavoriteMovies, onCardClicked: (movie: String) -> Unit) {
            binding.ivMovieItem.load(movie.Poster)
            binding.tvTitle.text = movie.Title
            binding.root.setOnClickListener {
                onCardClicked(movie.imdbID)
            }
//            binding.tvTitle.setOnClickListener {
//                val action = FavoritesFragmentDirections.actionFavoritesFragmentToOneMovieFragment(movie.imdbID)
//                controller.navigate(action)
//            }
            binding.tvYear.text = movie.Year
            binding.tvType.text = movie.Type

            with(binding){
                btnFavorite.run {
                    setBackgroundColor(Color.RED)
                    text = context.getString(R.string.delete)
                    setOnClickListener {
                        onDeleteFavorite(movie)
                    }
                }
            }
        }
    }
}