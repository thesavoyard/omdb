package com.example.omdb.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.omdb.databinding.MovieItemBinding
import com.example.omdb.model.local.FavoriteMovies
import com.example.omdb.model.remote.objects.Search

class FirstFragAdapter(
    val onSetFavorite: (movies: FavoriteMovies) -> Unit,
    private val navigateToOneMovie: (id: String) -> Unit
) : RecyclerView.Adapter<FirstFragViewHolder>() {
    private var searchResults: MutableList<Search> = mutableListOf()
    val TAG = "FirstFragAdapter"

    fun updateList(newList: List<Search>) {
        val oldSize = searchResults.size
        searchResults.clear()
        notifyItemRangeChanged(0, oldSize)
        searchResults.addAll(newList)
        notifyItemRangeChanged(0, newList.size)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FirstFragViewHolder {
        //Log.e(TAG, "onCreateViewHolder: We about to create this mutha fuckkaaa")
        return FirstFragViewHolder(
            MovieItemBinding.inflate(LayoutInflater.from(parent.context), parent, false),
            onSetFavorite
        )
    }

    override fun onBindViewHolder(holder: FirstFragViewHolder, position: Int) {
        //Log.e(TAG, "onBindViewHolder: binding the view at position $position")
        holder.displayMovies(searchResults[position], navigateToOneMovie)

    }

    override fun getItemCount(): Int {
        //Log.e(TAG, "getItemCount: Getting the size")
        return searchResults.size
    }


}

class FirstFragViewHolder(
    private val binding: MovieItemBinding,
    val onSetFavorite: (movies: FavoriteMovies) -> Unit
) :
    RecyclerView.ViewHolder(binding.root) {
    val TAG = "FirstFragViewHolder"
    fun displayMovies(movie: Search, onCardClicked: (movie: String) -> Unit) {
        //Log.e(TAG, "displayMovies: displaying movie $movie")
        binding.ivMovieItem.load(movie.Poster)
        binding.tvTitle.text = movie.Title
        binding.root.setOnClickListener {
            onCardClicked(movie.imdbID)
        }
        binding.tvYear.text = movie.Year
        binding.tvType.text = movie.Type
        binding.btnFavorite.setOnClickListener {
            //Log.e(TAG, "displayMovies: ${movie.toEntity()}")
            onSetFavorite(movie.toEntity())
        }
    }


}



