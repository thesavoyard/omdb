package com.example.omdb.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.omdb.model.OMDBRepo

class MoviesVMFactory(
    private val repo: OMDBRepo
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MoviesViewModel(repo) as T
    }
}