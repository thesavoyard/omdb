package com.example.omdb.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.omdb.model.OMDBRepo
import com.example.omdb.model.local.FavoriteMovies
import com.example.omdb.view.state.MoviesState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MoviesViewModel @Inject constructor(private val repo: OMDBRepo) : ViewModel() {

    private val _movies: MutableLiveData<MoviesState> = MutableLiveData(MoviesState())

    val movies: LiveData<MoviesState> get() = _movies

    fun getOneMovie(search: String) {
        viewModelScope.launch {
            val resposne = repo.getOneMovie(search)
            _movies.value = MoviesState(
                oneMovie = resposne
            )
        }
    }

    fun saveFavoriteMovies(movies: FavoriteMovies) {
        viewModelScope.launch {
            repo.saveFavoriteMovies(movies)
//            _movies.value = MoviesState(
//                favoriteMovies = response
//            )
        }
    }

    fun getFavoriteMovies() {
        viewModelScope.launch {
            val response = repo.getFavoriteMovies()
            _movies.value = MoviesState(
                favoriteMovies = response
            )
        }
    }

    fun deleteMovie(movie: FavoriteMovies) {
        viewModelScope.launch {
            repo.deleteMovie(movie)
            val response = repo.getFavoriteMovies()
            _movies.value = MoviesState(
                favoriteMovies = response
            )
        }
    }

    fun getMovies(search: String) {
        viewModelScope.launch {
            val response = repo.getMovies(search)
            _movies.value = MoviesState(
                searchResults = response
            )
        }
    }

    fun getFilteredFavorites(search: String) {
        viewModelScope.launch {
            val response = repo.getFilteredFavorites(search)
            Log.e("TAG", "getFilteredFavorites response object: $response")
            _movies.value = MoviesState(
                favoriteMovies = response
            )
        }
    }
}


