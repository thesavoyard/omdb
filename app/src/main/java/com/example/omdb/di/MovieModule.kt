package com.example.omdb.di

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import com.example.omdb.model.OMDBRepo
import com.example.omdb.model.local.FavoriteMovieDao
import com.example.omdb.model.local.FavoriteMoviesDB
import com.example.omdb.model.remote.OMDBService
import com.example.omdb.view.adapters.FirstFragAdapter
import com.example.omdb.viewmodel.MoviesVMFactory
import com.example.omdb.viewmodel.MoviesViewModel
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ViewModelScoped
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class MovieModule {
    //viewmodel
    @Provides
    fun providesViewmodel(vmFactory: MoviesVMFactory): MoviesViewModel = vmFactory.create(MoviesViewModel::class.java)

    //vmfactory
    @Provides
    fun providesVMFactory(repo: OMDBRepo): MoviesVMFactory = MoviesVMFactory(repo)

    //adapter example:
//    @Provides
    //@ActivityScoped
//    fun providesAdapter(): FirstFragAdapter = FirstFragAdapter()
    //repo
    @Provides
    @Singleton
    fun providesRepo(service: OMDBService, dao: FavoriteMovieDao): OMDBRepo = OMDBRepo(service, dao)
    //service
    @Provides
    @Singleton
    fun getOMDBService(): OMDBService = OMDBService.getInstance()
    //dao
    @Provides
    fun getDao(favoriteMoviesDB: FavoriteMoviesDB): FavoriteMovieDao = favoriteMoviesDB.favoriteMovieDao()
    //room database
    @Provides
    @Singleton
    fun getDatabase(@ApplicationContext context: Context): FavoriteMoviesDB = FavoriteMoviesDB.getInstance(context)

}