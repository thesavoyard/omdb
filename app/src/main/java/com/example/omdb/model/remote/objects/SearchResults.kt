package com.example.omdb.model.remote.objects

data class SearchResults(
    val Response: String,
    val Search: List<Search>,
    val totalResults: String,
    val Error: String?
)