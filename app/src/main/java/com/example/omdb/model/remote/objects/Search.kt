package com.example.omdb.model.remote.objects

import com.example.omdb.model.local.FavoriteMovies

data class Search(
    val Poster: String,
    val Title: String,
    val Type: String,
    val Year: String,
    val imdbID: String
) {
    fun toEntity() = FavoriteMovies(
        Title = Title,
        Year = Year,
        imdbID = imdbID,
        Type = Type,
        Poster = Poster
    )
}

