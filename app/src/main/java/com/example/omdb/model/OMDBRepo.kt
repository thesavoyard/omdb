package com.example.omdb.model

import android.content.Context
import android.util.Log
import com.example.omdb.model.local.FavoriteMovieDao
import com.example.omdb.model.local.FavoriteMovies
import com.example.omdb.model.local.FavoriteMoviesDB
import com.example.omdb.model.remote.OMDBService
import com.example.omdb.model.remote.objects.IMDBResponse
import com.example.omdb.model.remote.objects.Search
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton


class OMDBRepo @Inject constructor(
    val omdbService: OMDBService,
    val favoriteMovieDao: FavoriteMovieDao
) {

    suspend fun getMovies(search: String): List<Search> = withContext(Dispatchers.IO) {
        val remoteMovies = omdbService.getSearchResults(search = search)
        val status: Boolean = remoteMovies.Response.toBoolean()
        return@withContext if (!status) {
            listOf()
        } else {
            remoteMovies.Search
        }
    }

    suspend fun getOneMovie(search: String): IMDBResponse = withContext(Dispatchers.IO) {
        return@withContext omdbService.getOneMovie(search = search)
    }

    suspend fun saveFavoriteMovies(movies: FavoriteMovies): List<FavoriteMovies> {
        favoriteMovieDao.insertMovies(movies)
        return favoriteMovieDao.getAll()
    }

    suspend fun getFavoriteMovies(): List<FavoriteMovies> = withContext(Dispatchers.IO) {
        return@withContext favoriteMovieDao.getAll()
    }

    suspend fun getFilteredFavorites(search: String): List<FavoriteMovies> =
        withContext(Dispatchers.IO) {
            Log.e("TAG", "getFilteredFavorites: $search", )
            return@withContext favoriteMovieDao.getSearched(search)
        }

    suspend fun deleteMovie(movie: FavoriteMovies) = withContext(Dispatchers.IO) {
        favoriteMovieDao.deleteMovie(movie)
    }
}