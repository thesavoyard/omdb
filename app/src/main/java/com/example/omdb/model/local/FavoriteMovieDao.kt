package com.example.omdb.model.local

import androidx.room.*

@Dao
interface FavoriteMovieDao {

    @Query("SELECT * FROM FavoriteMovies")
    suspend fun getAll(): List<FavoriteMovies>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMovies(vararg movies: FavoriteMovies)

//    @Query("SELECT * FROM FavoriteMovies WHERE id=:id")
//    fun getOne(id: Int): List<FavoriteMovies>

    @Query("SELECT * FROM FavoriteMovies WHERE Title LIKE '%' || :search || '%'")
    fun getSearched(search: String): List<FavoriteMovies>

    @Delete
    suspend fun deleteMovie(movie: FavoriteMovies)
}