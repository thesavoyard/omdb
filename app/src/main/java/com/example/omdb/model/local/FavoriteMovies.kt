package com.example.omdb.model.local

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class FavoriteMovies(
    val Title:String = "n/a",
    val Year: String = "n/a",
    @PrimaryKey(autoGenerate = false)
    val imdbID: String = "n/a",
    val Type: String = "n/a",
    val Poster: String = "n/a"
)
