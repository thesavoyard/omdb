package com.example.omdb.model.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [FavoriteMovies::class], version = 1)
abstract class FavoriteMoviesDB: RoomDatabase() {

    abstract fun favoriteMovieDao(): FavoriteMovieDao

    companion object {
        private const val DATABASE_NAME = "FavoriteMovies.db"

        private var instance:FavoriteMoviesDB? = null

        fun getInstance(context: Context): FavoriteMoviesDB {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also {
                    instance = it
                }
            }
        }

        private fun buildDatabase(context: Context): FavoriteMoviesDB {
            return Room
                .databaseBuilder(context, FavoriteMoviesDB::class.java, DATABASE_NAME)
                .build()
        }
    }
}