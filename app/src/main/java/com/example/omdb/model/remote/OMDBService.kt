package com.example.omdb.model.remote

import com.example.omdb.model.remote.objects.IMDBResponse
import com.example.omdb.model.remote.objects.SearchResults
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface OMDBService {

    companion object {
        private const val BASE_URL = "https://www.omdbapi.com"
        const val API_KEY = "5900a8bc"
        const val SEARCH = "/"
        private val interceptor = HttpLoggingInterceptor()
            .also {
                it.setLevel(HttpLoggingInterceptor.Level.BODY)
            }

        private val client: OkHttpClient = OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .build()

        fun getInstance(): OMDBService = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create()
    }


    @GET(SEARCH)
    suspend fun getSearchResults(
        @Query("apikey") key: String = API_KEY,
        @Query("s") search: String
    ): SearchResults

    @GET(SEARCH)
    suspend fun getOneMovie(
        @Query("apikey") key: String = API_KEY,
        @Query("i") search: String
    ): IMDBResponse

}


//interface MovieService {
//    companion object {
//        private const val BASE_URL: String = "https://www.omdbapi.com"
//        const val SEARCH = "/"//?apikey=&s={search}
//        const val API_KEY = "5aa7634e"
//        private val interceptor = HttpLoggingInterceptor()
//            .also {
//                it.setLevel(HttpLoggingInterceptor.Level.BODY)
//            }
//        private val client: OkHttpClient = OkHttpClient.Builder()
//            .addInterceptor(interceptor)
//            .build()
//
//        fun getInstance(): MovieService = Retrofit.Builder()
//            .baseUrl(BASE_URL)
//            .client(client)
//            .addConverterFactory(GsonConverterFactory.create())
//            .build()
//            .create()
//    }
//
//    @GET(SEARCH)
//    suspend fun getMovies(@Query("apikey")key:String = API_KEY, @Query("s") search: String): SearchResponse