package com.example.omdb.model.remote.objects

data class Rating(
    val Source: String,
    val Value: String
)